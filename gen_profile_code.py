threads = [1, 2, 3, 4, 5, 6, 7, 8, 12]
iters = 1000
N = 100000000

for th in threads:
    print(f'    std::cout << "started - threads: " << {th} << std::endl;')
    print(f'    settings.ThreadCount = {th};')
    print(f'    runSmooth(N, settings);')
    print()