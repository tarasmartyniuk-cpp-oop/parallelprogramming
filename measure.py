from subprocess import *
import re

proc_counts = [2, 3, 4, 5, 6, 7, 8, 12]
num_threads = 1
num_iters = 10000000
N = 100

for pc in reversed(proc_counts):
    print(f'start for {pc} procs')
    p = Popen(f'C:\dev\libs\ms-mpi\Bin\mpiexec.exe -np {pc} ./Release/ParallelLabs.exe -t {num_threads} -i {num_iters} -n {N}', 
        stdout=PIPE, stderr=PIPE)
    p.wait()

    if (p.returncode != 0):
        print(f'Error code: {p.returncode}, msg: {p.stderr.readlines()}')
    else:
        output = [b.decode('utf-8') for b in p.stdout.readlines()]
        print(''.join(output))
        # r = re.search('([0-9]+.[0-9]+)', output)
        # d = float(r[0])
        # print(d)






