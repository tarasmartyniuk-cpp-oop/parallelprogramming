#pragma once
#include "sort.h"
#include <atomic>
#include <cassert>
#include <debug_output.h>
#include <omp.h>
#include <utils.h>
#include <vector>

void myMerge(std::vector<int>& values, std::vector<int>& helper, int mergeStartThreadId, size_t threadCount, int chunksPerMerge);

template <class TSorter>
void sort(std::vector<int>& values, int inThreadCount, std::vector<int>& helper, TSorter&& sorter)
{
    int const N = values.size();
    omp_set_num_threads(inThreadCount);

#ifdef D
    std::atomic<size_t> threadCountFirst = 0;
#endif

#pragma omp parallel
    {
        int const threadId = omp_get_thread_num();
        int const threadCount = omp_get_num_threads();
#ifdef D
        threadCountFirst = threadCount;
#endif

        auto& [chunkStart, chunkEnd] = getChunkBounds(threadId, threadCount, N);
        sorter(values.begin() + chunkStart, values.begin() + chunkEnd);
    }

    std::atomic<bool> stop = false;
    size_t chunksPerMerge = 0;
    for (size_t i = 1;; i++)
    {
        chunksPerMerge = i * 2;

        if (chunksPerMerge > inThreadCount)
        {
            break;
        }

        //for (int j = 0; j < threadCount; j++)
#pragma omp parallel
        {
            //int const threadId = j;
            //size_t const threadCount = threadCountFirst;
            int const threadCount = omp_get_num_threads();
            int const threadId = omp_get_thread_num();
#ifdef D
            assert(threadCount == threadCountFirst);
#endif
            assert(threadCount == inThreadCount);

            bool const isMergingThread = threadId % chunksPerMerge == 0;
            if (isMergingThread)
            {
                myMerge(values, helper, threadId, threadCount, chunksPerMerge);
            }
        }
    }

    myMerge(values, helper, 0, inThreadCount, chunksPerMerge);
}

template <class TSorter>
void profileSort(std::vector<int>& values, size_t threadCount, TSorter&& sorter)
{
    std::cout << threadCount << ": ";
    printCallMeasure([&values, threadCount, &sorter] {
        sort(values, threadCount, sorter);
    });
}

template <class TSorter>
void profileSort(int inputSize, TSorter&& sorter)
{
    std::vector<int> inputs;
    std::vector<int> helper;

    auto generate = [&inputs, &helper, inputSize] {
        inputs = generateRandomVector<int>(inputSize);
        helper = inputs;
    };
    print("generating...");
    printCallMeasure(generate);

    print("---------------- started: ", ", N: ", inputSize, "---------------------");

    for (size_t th : {1, 2, 3, 4, 5, 6, 7, 8})
    {
        auto inputCopy = inputs;
        auto helperCopy = helper;

        print("---------------- started sort: threads: ", th, ", N: ", inputCopy.size(), "---------------------");
        printCallMeasure([&] {
            sort(inputCopy, th, helperCopy, sorter);
        });
    }
}

void testSort(std::vector<int>& values, size_t threadCount);

template <class TSorter>
void testSort(std::vector<int>& values, size_t threadCount, TSorter&& sorter)
{
    auto& sortedSingleThread = values;
    auto sortedMt = sortedSingleThread;

    sorter(sortedSingleThread.begin(), sortedSingleThread.end());

    auto helper = sortedMt;
    sort(sortedMt, threadCount, helper, sorter);

    bool eq = sortedSingleThread == sortedMt;
    assert(eq);
}


