#include "sort_parallel.h"

void testSort(std::vector<int>& values, size_t threadCount)
{
    auto selectionValues = values;

    using It = decltype(selectionValues)::iterator;
    testSort(values, threadCount , bubbleSort<It>);
    testSort(selectionValues, threadCount, selectionSort<It>);
}

void myMerge(std::vector<int> &values, std::vector<int>& helper, int mergeStartThreadId, size_t threadCount, int chunksPerMerge)
{
    int lastChunkId = mergeStartThreadId + chunksPerMerge - 1;
    if (lastChunkId >= threadCount)
    {
        lastChunkId = threadCount - 1;
    }

    int const actualChunksInMerge = lastChunkId - mergeStartThreadId + 1;
    assert(actualChunksInMerge > 0);
    if (actualChunksInMerge == 1)
    {
        return;
    }

    auto& [leftHalfStart, _] = getChunkBounds(mergeStartThreadId, threadCount, values.size());
    auto& [__, rightHalfEnd] = getChunkBounds(lastChunkId, threadCount, values.size());

    //int const firstRightChunkId = mergeStartThreadId + (chunksPerMerge / 2);
    int const firstRightChunkId = mergeStartThreadId + (actualChunksInMerge / 2);
    assert(firstRightChunkId != mergeStartThreadId);

    auto& [rightHalfStart, ___] = getChunkBounds(firstRightChunkId, threadCount, values.size());

    debug("Merge:");
    PRINT_VAR(chunksPerMerge);
    debug("chunksPerMerge: ", chunksPerMerge, " (actual:", actualChunksInMerge, " )");
    PRINT_VAR(leftHalfStart);
    PRINT_VAR(rightHalfStart);
    PRINT_VAR(rightHalfEnd);

    auto start = values.begin() + leftHalfStart;
    auto end = values.begin() + rightHalfEnd; 

    std::copy(start, end, helper.begin() + leftHalfStart);

    std::merge(helper.begin() + leftHalfStart, helper.begin() + rightHalfStart,
         helper.begin() + rightHalfStart, helper.begin() + rightHalfEnd,
         values.begin() + leftHalfStart);

    //std::inplace_merge(start,
    //    values.begin() + rightHalfStart,
    //    end);
};