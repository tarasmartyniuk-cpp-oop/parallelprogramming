#pragma once
#include <algorithm>
#include <vector>
#include <cassert>


//inline void bubbleSort(std::vector<int>& values, size_t from, size_t to)
//{
//    //std::sort(values.begin() + from, values.begin() + to);
//
//    auto n = to;
//    for (size_t i = from; i < n - 1; i++)
//
//        // Last i elements are already in place  
//        for (int j = from; j < n - i - 1; j++)
//            if (values[j] > values[j + 1])
//                std::swap(values[j], values[j + 1]);
//}

/// Bubble Sort, allow user-defined less-than operator
template <typename iterator, typename LessThan>
void bubbleSortHelper(iterator first, iterator last, LessThan lessThan)
{
    if (first == last)
        return;
    // usually "last" points beyond the last element
    // now it points directly to that last element
    --last;
    // only one element => it's sorted
    if (first == last)
        return;
    bool swapped;
    do
    {
        // reset swapped flag
        swapped = false;
        iterator current = first;
        while (current != last)
        {
            // bubble up
            iterator next = current;
            ++next;
            // two neighbors in wrong order ? swap them !
            if (lessThan(*next, *current))
            {
                std::iter_swap(current, next);
                swapped = true;
            }
            ++current;
        }
        // last element is already sorted now
        --last; // remove this line if you only have a forward iterator
        // last will move closer towards first
    } while (swapped && first != last);
}
/// Bubble Sort with default less-than operator
template <typename iterator>
void bubbleSort(iterator first, iterator last)
{
    bubbleSortHelper(first, last,
        std::less<typename std::iterator_traits<iterator>::value_type>());
}

/// Selection Sort, allow user-defined less-than operator
template <typename iterator, typename LessThan>
void selectionSort(iterator first, iterator last, LessThan lessThan)
{
    for (iterator current = first; current != last; ++current)
    {
        // find smallest element in the unsorted part and remember its iterator in "minimum"
        iterator minimum = current;
        iterator compare = current;
        ++compare;
        // walk through all still unsorted elements
        while (compare != last)
        {
            // new minimum found ? save its iterator
            if (lessThan(*compare, *minimum))
                minimum = compare;
            // next element
            ++compare;
        }
        // add minimum to the end of the already sorted part
        if (current != minimum)
            std::iter_swap(current, minimum);
    }
}
/// Selection Sort with default less-than operator
template <typename iterator>
void selectionSort(iterator first, iterator last)
{
    selectionSort(first, last,
        std::less<typename std::iterator_traits<iterator>::value_type>());
}
