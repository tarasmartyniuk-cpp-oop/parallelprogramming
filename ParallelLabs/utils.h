// copyright 2019 Taras Martyniuk
#pragma once
#include <algorithm>
#include <chrono>
#include <iostream>
#include <functional>
#include <cassert>
#include <random>


static bool AreEqualWithEpsilon(float left, float right)
{
    return std::abs(left - right) < std::numeric_limits<float>::epsilon();
}

template <class FloatContainer>
bool AreContainersEqualWithEpsilon(FloatContainer const& left, FloatContainer const& right)
{
    return std::equal(left.cbegin(), left.cend(), right.cbegin(), &AreEqualWithEpsilon);
}

inline std::pair<size_t, size_t> getChunkBounds(int chunkIndex, int chunksCount, int N)
{
    assert(chunkIndex < chunksCount);

    int const chunkSize = (N / chunksCount);

    int chunkStartIndex = chunkIndex * chunkSize;
    int chunkEndIndex = chunkStartIndex + chunkSize;
    if (chunkIndex == chunksCount - 1)
    {
        // last chunk extended up to the end
        chunkEndIndex = N;
    }

    return {chunkStartIndex, chunkEndIndex};
}

template <class TElem>
std::vector<TElem> generateRandomVector(size_t inputSize)
{
    auto fillRand = [inputSize](auto const& rand) {
        std::random_device rd;
        std::mt19937 gen(rd());

        std::vector<TElem> inputs(inputSize);
        for (size_t i = 0; i < inputSize; i++)
        {
            inputs.at(i) = rand(gen);
        }
        return inputs;
    };

    if constexpr (std::is_integral<TElem>::value)
    {
        std::uniform_int_distribution<TElem> rand(0, inputSize);
        return fillRand(rand);
    }
    else if constexpr (std::is_floating_point<TElem>::value)
    {
        std::uniform_real_distribution<TElem> rand(0.0, (float)inputSize);
        return fillRand(rand);
    }
}

template <class TCallable>
long long measureCallDuration(TCallable&& callable)
{
    auto t1 = std::chrono::high_resolution_clock::now();
    callable();
    auto t2 = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    return duration;
}

template <class TCallable>
void printCallMeasure(TCallable&& callable)
{
    long long milliseconds = measureCallDuration(callable);
    double seconds = double(milliseconds) / 1000.f;

    std::cout.precision(4);
    std::cout << milliseconds << " ms; " << seconds << " s\n";
}

