#pragma once
#include <vector>
#include <iostream>

class Matrix
{
  public:
    Matrix(size_t rows, size_t cols)
        : RowsCount(rows)
        , ColumnsCount(cols)
        , m_values(rows * cols)
    {}

    Matrix(std::pair<size_t, size_t> dimentions)
        : Matrix(dimentions.first, dimentions.second)
    {}

    size_t RowsCount = 0;
    size_t ColumnsCount = 0;

    float& at(int row, int col)
    {
        return m_values.at(row * ColumnsCount + col);
    }

    float at(int row, int col) const
    {
        return m_values.at(row * ColumnsCount + col);
    }

    std::vector<float> const& values() { return m_values; }

  private:
    std::vector<float> m_values;
};

inline std::ostream& operator<<(std::ostream& os, Matrix const& mat)
{
    os.precision(2);
    os << "[";
    for (size_t r = 0; r < mat.RowsCount; r++)
    {
        os << "  [";
        for (size_t c = 0; c < mat.ColumnsCount; c++)
        {
            os << std::fixed << mat.at(r, c) << "\t";
        }
        if (r == mat.RowsCount - 1)
        {
            os << "]]\n";
        }
        else
        {
            os << "]\n";
        }
    }
    return os;
}
