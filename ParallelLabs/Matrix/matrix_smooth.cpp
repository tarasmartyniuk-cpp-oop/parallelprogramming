#include "matrix_smooth.h"
#include "utils.h"
#include <debug_output.h>
#include <omp.h>

void smoothMatrix(Matrix& original, Matrix& helper, Matrix const& fixedElements, SmoothSettings settings)
{
    debug("started for iters: ", settings.IterCount, ", threads: ", settings.ThreadCount);

#pragma omp parallel
    {
        auto& [chunkStartRow, chunkEndRow] = getChunkBounds(omp_get_thread_num(), omp_get_num_threads(), original.RowsCount);

        for (size_t r = chunkStartRow; r < chunkEndRow; r++)
        {
            for (size_t c = 0; c < fixedElements.ColumnsCount; c++)
            {
                float const fixed = fixedElements.at(r, c);
                original.at(r, c) = fixed;
                helper.at(r, c) = fixed;
            }
        }
    }

    //PRINT_VAR(original);

    for (size_t i = 0; i < settings.IterCount; i++)
    {
        smoothMatrixIter(original, helper, fixedElements, settings.ThreadCount);
    }
}

void smoothMatrixIter(Matrix& original, Matrix& smoothed, Matrix const& fixedElements, size_t inThreadCount)
{
    omp_set_num_threads(inThreadCount);
#pragma omp parallel
    //for (int j = 0; j < inThreadCount; j++)
    {
        size_t const lastCol = original.ColumnsCount - 1;

        //int const threadId = j;
        //size_t const threadCount = inThreadCount;
        int const threadId = omp_get_thread_num();
        int const threadCount = omp_get_num_threads();

        auto& [chunkStartRow, chunkEndRow] = getChunkBounds(threadId, threadCount, original.RowsCount);
        if (chunkStartRow == 0)
        {
            chunkStartRow = 1;
        }
        if (chunkEndRow == original.RowsCount)
        {
            chunkEndRow--;
        }

        for (size_t r = chunkStartRow; r < chunkEndRow; r++)
        {
            for (size_t c = 1; c < lastCol; c++)
            {
                //bool const isFixed = std::any_of(fixedElements.cbegin(), fixedElements.cend(),
                //                                 [r, c](FixedElement el) { return el.Row == r && el.Column == c; });
                if (fixedElements.at(r, c) != 0.f)
                {
                    continue;
                }
                smoothed.at(r, c) = (original.at(r - 1, c) + original.at(r, c) + original.at(r + 1, c) +
                                     original.at(r, c - 1) + original.at(r, c + 1)) /
                                    5.f;
            }
        }
    }

    original = smoothed;
}

void generateFixedElements(Matrix& outMatrix, float fixedPercent)
{
    for (size_t i = 0; i < outMatrix.RowsCount; i++)
    {
        for (size_t j = 0; j < outMatrix.ColumnsCount; j++)
        {
            bool const isFixed = ((float)rand() / (float)RAND_MAX) > (1.f - fixedPercent);
            if (isFixed)
            {
                outMatrix.at(i, j) = 42.f;
            }
        }
    }
}

void testSmoothMatrix(std::pair<size_t, size_t> matrixDimentions, Matrix const& fixedElements, SmoothSettings settings)
{
    Matrix matrix(matrixDimentions.first, matrixDimentions.second);
    Matrix singleThread = matrix;
    Matrix helper = matrix;

    smoothMatrix(matrix, helper, fixedElements, settings);
    debug(matrix);

    smoothMatrix(singleThread, helper, fixedElements, {settings.IterCount, 1});

    assert(AreContainersEqualWithEpsilon(matrix.values(), singleThread.values()));
}

void profileMatrixSmooth(std::pair<size_t, size_t> matrixDimentions, size_t iterCount, float fixedPercent)
{
    auto& [rowCount, colCount] = matrixDimentions;
    Matrix fixedElements(rowCount, colCount);

    auto generate = [&] {
        generateFixedElements(fixedElements, fixedPercent);
    };
    print("generating...");
    printCallMeasure(generate);

    print("---------------- started: ", "Rows: ", rowCount, ", Columns: ", colCount, "---------------------");
    print("threads: time", "\n---------------------");

    Matrix const matrix(rowCount, colCount);
    Matrix helper = matrix;
    Matrix inputCopy = matrix;

    for (size_t th : {1, 2, 3, 4}) //, 5, 6, 7, 8})
    {
        inputCopy = matrix;
        helper = inputCopy;

        std::cout << th << " : ";
        printCallMeasure([&] {
            smoothMatrix(inputCopy, helper, fixedElements, {iterCount, th});
        });
    }
}