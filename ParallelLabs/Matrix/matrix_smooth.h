#pragma once
#include "Matrix.h"
#include "Smooth/SmoothSettings.h"


struct FixedElement
{
    size_t Row = 0;
    size_t Column = 0;
    float Value = 0;
};

inline std::ostream& operator<<(std::ostream& os, FixedElement const& el)
{
    os << "{r: " << el.Row << ", c: " << el.Column << ", val: " << el.Value << "}";
    return os;
}


// matrixDimentions: rows, columns
void smoothMatrix(Matrix& original, Matrix& helper, Matrix const& fixedElements, SmoothSettings settings);
void smoothMatrixIter(Matrix& original, Matrix& helper, Matrix const& fixedElements, size_t threadCount);

void generateFixedElements(Matrix& outMatrix, float fixedPercent);
void testSmoothMatrix(std::pair<size_t, size_t> matrixDimentions, Matrix const& fixedElements, SmoothSettings settings);

void profileMatrixSmooth(std::pair<size_t, size_t> matrixDimentions, size_t iterCount, float fixedPercent);
