#include "Smooth/smooth.h"
#include "Smooth/smooth_mpi.h"
#include <Matrix/matrix_smooth.h>
#include <Sort/sort.h>
#include <Sort/sort_parallel.h>
#include <argparse.h>
#include <cassert>
#include <chrono>
#include <debug_output.h>
#include <easy/profiler.h>
#include <mpi.h>
#include <mpi_utils.h>
#include <optional>
#include <utils.h>


std::optional<std::pair<SmoothSettings, size_t>> parseSmoothInput(int argc, char** argv)
{
    ArgumentParser options("Smooth program");
    options.add_argument("-t", "thread count", true);
    options.add_argument("-i", "iters count", true);
    options.add_argument("-n", "input size", true);

    try
    {
        options.parse(argc, argv);
    }
    catch (const ArgumentParser::ArgumentNotFound& ex)
    {
        std::cout << ex.what() << std::endl;
        return std::nullopt;
    }

    if (options.is_help())
    {
        return std::nullopt;
    }

    return std::make_pair(
        SmoothSettings{
            options.get<size_t>("i"),
            options.get<size_t>("t")},
        options.get<size_t>("n"));
}

void merge(int* const leftOrig, int* const rightOrig, int* const end)
{
    auto left = leftOrig;
    int* leftSwapped = nullptr;
    auto right = rightOrig;
    auto mergedIt = leftOrig;

    int* swappedEnd = nullptr;


    while (left != right && leftSwapped != swappedEnd && right != end)
    {
        if (*left < *right)
        {
            bool swapped = leftSwapped;
            assert(left == mergedIt + 1);
            if (swapped)
            {
                std::swap(*left, *leftSwapped);
                left++;
            }
        }
    }
}

int main(int argc, char** argv)
{
    if (MPI_Init(&argc, &argv) != MPI_SUCCESS)
    {
        std::cout << "that's bad";
    }

    //int N = 100 * 100000;
    //std::vector<int> v{1, 10, 12, 1, 2, 3, 4, 5, 6, 45 ,23 ,21};

    //auto bub2 = bub1;
    //using It = std::vector<int>::iterator;
    //print("bubble:\n");
    //profileSort(N, bubbleSort<It>);
    //profileSort(N * 2, bubbleSort<It>);

    //auto sorter = [](auto first, auto last) {
    //    std::sort(first, last);
    //};

    //print("selectionSort:\n");
    //profileSort(N, selectionSort<It>);
    //profileSort(N * 2, selectionSort<It>);

    //profileSort(N, sorter);
    //profileSort(N * 2, sorter);


    // ---------- smooth CLI
    //auto& [settings, _] = std::make_pair(
    //auto& [settings, N] = *parseSmoothInput(argc, argv);
    //    SmoothSettings{
    //        10,
    //        1,
    //    },
    //    17);


    //std::vector<float> v(9);
    //v.at(0) = 1;
    //v.at(v.size() / 2) = 1;
    //v.at(v.size() - 1) = 1;

    //smooth(v, settings);

    //startSmoothMpi(settings, N);

    auto const matrixDims = std::make_pair(1 * 1000, 10000);

    //std::vector<FixedElement> fixedElements{
    //    { 1, 1, 42}, { 5, 5, 42}, { 8, 8, 42}};
    //Matrix fixedElements (matrixDims);
    //generateFixedElements(fixedElements, 0.1f);
    //testSmoothMatrix(matrixDims, fixedElements, {15, 4});
    profileMatrixSmooth(matrixDims, 15, 0.1f);
}