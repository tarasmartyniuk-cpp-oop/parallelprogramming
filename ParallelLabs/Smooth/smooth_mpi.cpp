#include "smooth_mpi.h"
#include <Windows.h>
#include <cassert>
#include <debug_output.h>
#include <mpi.h>
#include <mpi_utils.h>
#include <processthreadsapi.h>
#include <thread>

enum class MessageTag
{
    ChunkSize,
    Chunk,              // 1
    LeftBorder,         // 2
    RightBorder,        // 3
    ProcessedChunkSize, // 4
    ProcessedChunk      // 5
};

#define D

inline std::pair<size_t, size_t> getChunkBoundsWithExtraCorners(size_t chunkIndex, size_t chunksCount, size_t N)
{
    int const chunkSize = (N / chunksCount);

    int chunkStartIndex = chunkIndex * chunkSize;
    int chunkEndIndex = chunkStartIndex + chunkSize;
    // we add one extra on both sides, except on array borders
    if (chunkIndex != 0)
    {
        --chunkStartIndex;
    }
    if (chunkIndex == chunksCount - 1)
    {
        // last chunk extended up to the end
        chunkEndIndex = N;
    }
    else
    {
        ++chunkEndIndex;
    }

    return {chunkStartIndex, chunkEndIndex};
}

void startSmoothMpi(SmoothSettings settings, size_t N)
{
//    if (!isMaster())
//    //if (localMpiRank() == 1)
//    {
//#ifdef D
//        mpiDebug("Start - process id: ", GetCurrentProcessId());
//        //while (true)
//        {
//            using namespace std::chrono_literals;
//            std::this_thread::sleep_for(1s);
//        }
//#endif
//    }

    if (isMaster())
    {
        print("Starting for Processes: ", getProcessCount(), " Iters: ", settings.IterCount, ", N: ", N);

        auto beMaster = [N, settings] {
            mpiDebug(" is master");
            //std::vector<float> values = {13.45, 7.75, 5.00, 4.69, 5.86, 15.35, 5.82, 6.18, 14.15, 16.57, 3.01, 1.84, 14.51, 16.92, 3.22, 16.61, 13.47};

            auto values = generateRandomVector<float>(N);
            //std::vector<float> values(9);
            //values.at(0) = 1;
            //values.at(values.size() / 2) = 1;
            //values.at(values.size() - 1) = 1;

#ifdef D
#endif // D
            auto correct = values;

            mpiDebug("input: ", values);
            smoothMpiMaster(values);

//#ifdef D
            smooth(correct, settings);

            mpiDebug("before assert:\n\n");
            PRINT_VAR(values);

            print(values);
            PRINT_VAR(correct);
            assert(AreContainersEqualWithEpsilon(values, correct));
//#endif
        };


        long long milliseconds = measureCallDuration(beMaster);
        double seconds = double(milliseconds) / 1000.f;

        print("\ttime: ", seconds);
    }
    else
    {
        mpiDebug(" is worker");
        smoothMpiWorker(settings);
    }

    mpiDebug("Finalized");
    MPI_Finalize();
}

void smoothMpiMaster(std::vector<float>& values)
{
    //LogMsg = true;

    assert(isMaster());
    size_t const N = values.size();
    if (N < 2)
    {
        return;
    }

    size_t const workersCount = getProcessCount() - 1;

    size_t const lastIndex = N - 1;
    for (size_t i = 0; i < workersCount; i++)
    {
        auto& [chunkStartIndex, chunkEndIndex] = getChunkBoundsWithExtraCorners(i, workersCount, N);
        mpiDebug("chunk for worker#", i, ": [", chunkStartIndex, ",", chunkEndIndex, ")");

        size_t const paddedChunkSize = chunkEndIndex - chunkStartIndex;

        std::vector<float> chunk(paddedChunkSize);
        std::copy(values.cbegin() + chunkStartIndex, values.cbegin() + chunkEndIndex, chunk.begin());

        size_t const workerRank = i;
        assert(!isMaster(workerRank));

        mpiSendVector(MessageTag::ChunkSize, MessageTag::Chunk, workerRank, chunk);
    }

#ifdef D
    std::fill(values.begin() + 1, values.end() - 1, 0.f);
    auto valuesCopy = values;
#endif

    // merge results
    for (size_t i = 0; i < workersCount; i++)
    {
        int workerRank = 0;
        std::vector<float> chunk = mpiRecvVector(MessageTag::ProcessedChunkSize, MessageTag::ProcessedChunk, workerRank);

        auto& [valuesStart, valuesEnd] = getChunkBoundsWithExtraCorners(workerRank, workersCount, N);
        size_t chunkStart = 0;
        size_t chunkEnd = chunk.size();

        if (workerRank != 0)
        {
            valuesStart++;
            chunkStart++;
        }
        if (workerRank != workersCount - 1)
        {
            chunkEnd--;
        }

        //#ifdef D
        //        for (size_t i = valuesStart; i < valuesEnd; i++)
        //        {
        //            if (!AreEqualWithEpsilon(values[i], valuesCopy[i]))
        //            {
        //                std::cout << "overriding other chunk, index: " << i << "\n";
        //                assert(!"overriding other chunk");
        //            }
        //        }
        //#endif

        mpiDebug("copying results #", workerRank, ": [", valuesStart, ",", valuesStart + (chunkEnd - chunkStart), ")");

        std::copy(chunk.cbegin() + chunkStart, chunk.cbegin() + chunkEnd,
                  values.begin() + valuesStart);
    }
}

void smoothMpiWorker(SmoothSettings settings)
{
    int masterRank = 0;
    LogMsg = false;
    auto chunk = mpiRecvVector(MessageTag::ChunkSize, MessageTag::Chunk, masterRank);

    std::vector<float> helper(chunk.size());
    auto const fixedElements = getFixedElements(chunk);

    for (size_t i = 0; i < settings.IterCount; i++)
    {
        smoothIteration(chunk, helper, fixedElements, settings.ThreadCount);

        int const leftWorkerRank = localMpiRank() - 1;
        int const rightWorkerRank = localMpiRank() + 1;

        if (localMpiRank() != 0)
        {
            assert(leftWorkerRank >= 0);
            mpiSend(chunk[1], leftWorkerRank, MessageTag::RightBorder);
        }
        bool isLastWorker = localMpiRank() == masterRank - 1;
        if (!isLastWorker)
        {
            assert(rightWorkerRank < masterRank);
            mpiSend(chunk[chunk.size() - 2], rightWorkerRank, MessageTag::LeftBorder);
        }

        if (!isLastWorker)
        {
            int const rightBorderIndex = chunk.size() - 1;
            mpiRecv(chunk[rightBorderIndex], MessageTag::RightBorder, rightWorkerRank);
            mpiDebug("adjusted right border, Idx: ", rightBorderIndex, "value: ", chunk[rightBorderIndex]);
        }
        if (localMpiRank() != 0)
        {
            mpiRecv(chunk[0], MessageTag::LeftBorder, leftWorkerRank);
            mpiDebug("adjusted left border, Idx: ", 0, "value: ", chunk[0]);
        }

        mpiDebug("I: ", i, " chunk: ", chunk);
    }

    mpiSendVector(MessageTag::ProcessedChunkSize, MessageTag::ProcessedChunk, masterRank, chunk);
}
