#include "smooth.h"
#include <algorithm>
#include <cassert>
#include <debug_output.h>
#include <iostream>
#include <omp.h>
#include <sstream>
#include <utils.h>

// smoothes the original one iteration,
void smoothIteration(std::vector<float>& original, std::vector<float>& helper, std::vector<bool> const& fixedElements, size_t threadCount)
{
    auto& smoothed = helper;

    omp_set_num_threads(static_cast<int>(threadCount));
    size_t const lastIndex = original.size() - 1;

    #pragma omp parallel for
    for (size_t i = 1; i < lastIndex; i++)
    {
        if (fixedElements[i])
        {
            smoothed[i] = original[i];
        }
        else
        {
            smoothed[i] = (original[i - 1] + original[i + 1]) / 2.f;
        }
    }

    auto copyIfFixed = [&](int index) {
        if (fixedElements[index])
        {
            smoothed[index] = original[index];
        }
    };

    copyIfFixed(0);
    copyIfFixed(lastIndex);

    std::swap(original, smoothed);

    PRINT_VAR_("smoothed", original);
}

std::vector<bool> getFixedElements(std::vector<float> const& values)
{
    std::vector<bool> fixedElements(values.size());
    for (int i = 0; i < values.size(); i++)
    {
        if (values[i] != 0.f)
        {
            fixedElements[i] = true;
        }
    }

    return fixedElements;
}

void smooth(std::vector<float>& values, SmoothSettings settings)
{
    size_t const N = values.size();
    if (N < 2)
    {
        return;
    }

    std::vector<float> helper(values.size());
    auto const fixedElements = getFixedElements(values);


    for (size_t i = 0; i < settings.IterCount; i++)
    {
        PRINT_VAR_("Iter: ", i);
        smoothIteration(values, helper, fixedElements, settings.ThreadCount);
    }
}

void smoothEdges(std::vector<float>& values, std::vector<float> const& orig)
{
    if (values.size() < 2)
    {
        return;
    }
    size_t const lastIndex = values.size() - 1;

    auto second = orig.at(1);
    auto secondLast = orig.at(lastIndex - 1);

    values[0] = second / 2.f;
    values[lastIndex] = secondLast / 2.f;
}

void runSmooth(std::vector<float>& values, SmoothSettings settings)
{
    //print("---------------- started: threads: ", settings.ThreadCount, ", iters: ", settings.IterCount, ", N: ", values.size(), "---------------------");
    //print(settings.ThreadCount, ": ");

    std::cout << settings.ThreadCount << ": ";

    auto smoothCall = [&values, settings] {
        smooth(values, settings);
    };
    print("working...");
    printCallMeasure(smoothCall);
}

void profileSmooth(int inputSize, SmoothSettings settings)
{
    std::vector<float> inputs;

    auto generate = [&inputs, inputSize] {
        inputs = generateRandomVector<float>(inputSize);
    };
    print("generating...");
    printCallMeasure(generate);

    print("---------------- started: ", ", iters: ", settings.IterCount, ", N: ", inputSize, "---------------------");


    for (size_t th : {1, 2, 3, 4, 5, 6, 7, 8, 12})
    {
        runSmooth(inputs, {settings.IterCount, th});
    }
}

void testSmooth(int inputSize, SmoothSettings settings)
{
    auto inputs = generateRandomVector<float>(inputSize);

    auto smoothedSingleThread = inputs;
    smooth(smoothedSingleThread, {settings.IterCount, 1});

    auto smoothedMt = inputs;
    smooth(smoothedMt, settings);

    bool eq = AreContainersEqualWithEpsilon(smoothedSingleThread, smoothedMt);
    assert(eq);
}

//std::vector<float> generateRandomVector(size_t inputSize)
//{
//    std::random_device rd;
//    std::mt19937 gen(rd());
//    std::uniform_real_distribution<float> rand(0.0, (float)inputSize);
//
//    std::vector<float> inputs(inputSize);
//    for (size_t i = 0; i < inputSize; i++)
//    {
//        inputs.at(i) = rand(gen);
//    }
//
//    return inputs;
//}
