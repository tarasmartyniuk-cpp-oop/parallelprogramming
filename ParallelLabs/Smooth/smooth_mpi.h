#pragma once
#include "smooth.h"

void startSmoothMpi(SmoothSettings settings, size_t N);
void smoothMpiMaster(std::vector<float>& values);
void smoothMpiWorker(SmoothSettings settings);