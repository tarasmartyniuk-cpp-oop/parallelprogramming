#pragma once
#include <type_traits>
#include <vector>
#include "SmoothSettings.h"

// openmp
std::vector<bool> getFixedElements(std::vector<float> const& values);
void smooth(std::vector<float>& values, SmoothSettings settings);
void smoothEdges(std::vector<float>& values, std::vector<float> const& orig);
void smoothIteration(std::vector<float>& original, std::vector<float>& helper, std::vector<bool> const& fixedElements , size_t threadCount);


void profileSmooth(int inputSize, SmoothSettings settings);
void testSmooth(int inputSize, SmoothSettings settings);


