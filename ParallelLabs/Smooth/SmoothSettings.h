#pragma once

struct SmoothSettings
{
    size_t IterCount = 0;
    size_t ThreadCount = 0;
};
