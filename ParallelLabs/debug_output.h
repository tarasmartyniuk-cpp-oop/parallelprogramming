#pragma once
#include "utils.h"
#include <iostream>
#include <unordered_map>
#include <sstream>

constexpr bool Printing =
#ifdef D
true;
#else
false;
#endif

template<class... Args>
inline void debug(Args... args)
{
    if constexpr (Printing)
    {
        print(args...);
    }
}

template <class T>
void print(T arg)
{
    std::stringstream ss;
    ss << arg << '\n';
    std::cout << ss.str();
    std::cout.flush();
}

// logs all params, adding newline after last
template<class T, class... Args>
inline void print(T currentArg, Args... args) // recursive variadic function
{
    std::stringstream ss;
    ss << currentArg;
    std::cout << ss.str();

    print(args...);
}

inline void print()
{
    std::cout << "\n";
}

#define PRINT_VAR_(varName, variable) debug(varName, ": ", variable)
#define PRINT_VAR(variable) PRINT_VAR_(#variable, variable)

template <class T>
std::ostream& operator<<(std::ostream& os, std::vector<T> const& vec)
{
    os.precision(2);
    os << "[";
    for (auto const& el : vec)
    {
        os << std::fixed << el << "\t";
    }
    os << "]";
    return os;
}

template <class TKey, class TValue>
std::ostream& operator<<(std::ostream& os, std::unordered_map<TKey, TValue> const& map)
{
    os << "{";
    for (const auto& [key, value] : map)
    {
        os << std::fixed << key << ": " << value << ",\t";
    }
    os << "}";
    return os;
}

template <class T>
std::string to_string(T* array, size_t size)
{
    std::stringstream ss;
    ss.precision(2);

    for (size_t i = 0; i < size; i++)
    {
        ss << std::fixed << array[i] << "\t";
    }
    ss << "]";
    return ss.str();
}
