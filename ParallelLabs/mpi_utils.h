#pragma once
#include <debug_output.h>
#include <mpi.h>

template <class T>
struct CppTypeToMpi
{};

#define CPP_TYPE_TO_MPI(cpp, mpi)               \
    template <>                                 \
    struct CppTypeToMpi<cpp>                    \
    {                                           \
        constexpr int mpiType() { return mpi; } \
    };

CPP_TYPE_TO_MPI(int, MPI_INT)
CPP_TYPE_TO_MPI(size_t, MPI_UNSIGNED_LONG)
CPP_TYPE_TO_MPI(float, MPI_FLOAT)

static bool LogMsg = false;


template <class TType, class TTag>
void mpiSend(TType payload, int destRank, TTag tag)
{
    mpiSend(&payload, 1, destRank, tag);
}

template <class TType, class TTag>
void mpiSend(TType* payload, int payloadSize, int destRank, TTag tag)
{
    constexpr int mpiType = CppTypeToMpi<TType>().mpiType();
#ifdef LOG_MSG
    if (LogMsg)
    {
        mpiDebug("Message sent: to: ", destRank, ", tag: ", static_cast<int>(tag), ", \n\tpayload: [", to_string(payload, payloadSize));
    }
#endif
    MPI_Send(payload, payloadSize, mpiType, destRank, static_cast<int>(tag), MPI_COMM_WORLD);
}

template <class TSizeTag, class TPayloadTag>
void mpiSendVector(TSizeTag sizeTag, TPayloadTag payloadTag, int destRank, std::vector<float>& payload)
{
    mpiSend(payload.size(), destRank, sizeTag);
    mpiSend(payload.data(), payload.size(), destRank, payloadTag);
}

template <class TType, class TTag>
void mpiRecv(TType& outPayload, TTag tag, int source = MPI_ANY_SOURCE)
{
    mpiRecvArray(&outPayload, 1, tag, source);
}

template <class TType, class TTag>
void mpiRecv(TType& outPayload, TTag tag, MPI_Status& outStatus, int source = MPI_ANY_SOURCE)
{
    mpiRecvArray(&outPayload, 1, tag, outStatus, source);
}

template <class TSizeTag, class TPayloadTag>
std::vector<float> mpiRecvVector(TSizeTag sizeTag, TPayloadTag payloadTag, int& outSenderRank)
{
    int size = 0;
    //int senderRank = 0;
    MPI_Status sizeStatus;
    mpiRecv(size, sizeTag, sizeStatus);
    outSenderRank = sizeStatus.MPI_SOURCE;

    std::vector<float> payload(size);
    MPI_Status arrayStatus;
    mpiRecvArray(payload.data(), size, payloadTag, arrayStatus, outSenderRank);

    return payload;
}

template <class TType, class TTag>
void mpiRecvArray(TType* outPayload, int payloadSize, TTag tag, int source = MPI_ANY_SOURCE)
{
    MPI_Status dummyStatus;
    mpiRecvArray(outPayload, payloadSize, tag, dummyStatus, source);
}

template <class TType, class TTag>
void mpiRecvArray(TType* outPayload, int payloadSize, TTag tag, MPI_Status& outStatus, int source = MPI_ANY_SOURCE)
{
    int const mpiType = CppTypeToMpi<TType>().mpiType();
#ifdef LOG_MSG
    if (LogMsg)
    {
        mpiDebug("awaiting msg: tag: \t", static_cast<int>(tag), ", source: \t", source);
    }
#endif

    int result = MPI_Recv(outPayload, payloadSize, mpiType, source, static_cast<int>(tag), MPI_COMM_WORLD, &outStatus);
#ifdef LOG_MSG
    if (LogMsg)
    {
        mpiDebug("Message recieved: tag: \t", outStatus.MPI_TAG, ", source: \t", outStatus.MPI_SOURCE, ", \n\tpayload: [", to_string(outPayload, payloadSize));
        if (result != MPI_SUCCESS)
        {
            mpiDebug("\t[ERROR]: ", outStatus.MPI_ERROR);
        }
    }
#endif
}

inline int localMpiRank()
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    return rank;
}

inline int getProcessCount()
{
    int processCount;
    MPI_Comm_size(MPI_COMM_WORLD, &processCount);
    return processCount;
}
// last process
inline int getMasterRank()
{
    return getProcessCount() - 1;
}

inline bool isMaster(int rank)
{
    return rank == getMasterRank();
}

inline bool isMaster()
{
    return isMaster(localMpiRank());
}

template <class... Args>
void mpiDebug(Args... args)
{
    auto s = isMaster() ? "[M]" : "";
    debug(s, "[PID: ", localMpiRank(), "]\t", args...);
}
